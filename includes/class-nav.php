<?php

class KKNav {
   
   /**
    * Retrieve the primary menu if one exists. Otherwise, build a list of the pages.
    * @param integer $depth
    * @param string $class
    * @param string $id
    * @param string $container
    */
   public static function primary($depth = 2, $class = 'left', $id = 'mainNav', $container = 'ul') {
      if( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary' ) ) {
         wp_nav_menu( array(
               'theme_location' => 'primary',
               'container' => $container,
               'menu_class' => $class,
               'menu_id' => $id,
               'depth' => $depth,
               'walker' => new Walker()
            )
         );
      } else {
         $current = is_front_page() ? 'class="current-menu-item"' : '';
         $output = '<'.$container.' id="'.$id.'" class="menu_class button-group '.$class.'">';
         $output .= '<li ' . $current . '><a title="Home" href="' . home_url( '/' ) . '">Home</a></li>';
         $output .= wp_list_pages( 'sort_column=menu_order&title_li=&echo=0&depth=2' );
         $output .= '</'.$container.'><!--endMainNav-->';
         echo $output;
      }
   }
   
   /**
    * Retrieve the mobile menu if one exists. Otherwise, build a list of the pages.
    * @param integer $depth
    * @param string $class
    * @param string $id
    * @param string $container
    */
   public static function mobile( $depth = 2, $class = 'left', $id = 'mobileNav', $container = 'ul' ) {
      if( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary' ) ) {
         wp_nav_menu( array(
               'theme_location' => 'mobile',
               'container' => $container,
               'menu_class' => $class,
               'menu_id' => $id,
               'depth' => $depth,
               'walker' => new Walker()
            )
         );
      } else {
         $current = is_front_page() ? 'class="current-menu-item"' : '';
         $output = '<'.$container.' id="'.$id.'" class="menu_class button-group '.$class.'">';
         $output .= '<li ' . $current . '><a title="Home" href="' . home_url( '/' ) . '">Home</a></li>';
         $output .= wp_list_pages( 'sort_column=menu_order&title_li=&echo=0&depth=2' );
         $output .= '</'.$container.'><!--endMobileNav-->';
         echo $output;
      }
   }
   
   /**
    * Retrieve the footer menu if one exists. Otherwise, build a list of the pages.
    * @param integer $depth
    * @param string $class
    * @param string $id
    * @param string $container
    */
   public static function footer( $depth = 1, $class = 'left', $id = 'footerNav', $container = 'ul') {
      if( function_exists( 'has_nav_menu' ) && has_nav_menu( 'footer' ) ) {
         wp_nav_menu( array(
               'theme_location' => 'footer',
               'container' => $container,
               'menu_class' => $class,
               'menu_id' => $id,
               'depth' => $depth,
               'walker' => new Walker()
            )
         );
      } else {
         $current = is_front_page() ? 'class="current-menu-item"' : '';
         $output = '<'.$container.' id="'.$id.'" class="menu_class inline-list '.$class.'">';
         $output .= '<li ' . $current . '><a title="Home" href="' . home_url( '/' ) . '">Home</a></li>';
         $output .= wp_list_pages( 'sort_column=menu_order&title_li=&echo=0&depth=1' );
         $output .= '</'.$container.'><!--endFooterNav-->';
         echo $output;
      }
   }
   
   /**
    * Retrieve the sitemap menu if one exists. Otherwise, build a list of the pages.
    * @param integer $depth
    * @param string $class
    * @param string $id
    * @param string $container
    */
   public static function sitemap( $depth = null, $class = 'left', $id = 'sitemapNav', $container = 'ul' ) {
      if( function_exists( 'has_nav_menu' ) && has_nav_menu( 'sitemap' ) ) {
         $args = array(
            'theme_location' => 'sitemap',
            'container' => $container,
            'menu_class' => $class,
            'menu_id' => $id
         );
         if( is_numeric( $depth ) ) {
            $args['depth'] = $depth;
         }
         wp_nav_menu( $args );
      } else {
         $current = ( is_front_page() ) ? 'class="current-menu-item"' : '';
         $output = '<ul id="sitemap" class="menu_class">';
         $output .= '<li ' . $current . '><a title="Home" href="' . home_url( '/' ) . '">Home</a></li>';
         $output .= wp_list_pages( 'sort_column=menu_order&title_li=&echo=0' );
         $output .= '</ul><!--endSitemap-->';
         echo $output;
      }
   }
}