<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
    <!--footer wrapper start here-->
    <div class="footer_wrapper">
    	<div class="float_left">
	<span class="copyright">&copy; Copyright <?php echo date('Y'); ?> Antonio's Restaurants. All Rights Reserved.</span><br />
        	<ul>
                <li><a href="<?php echo get_option('home'); ?>/terms-and-conditions" title="Terms &amp; Conditions">Terms &amp; Conditions</a></li>
                <li class="last"><a href="https://element5digital.com" title="Website Design"> Website Design</a></li>
            </ul>
        </div>
        
        <div class="float_right">
        	<ul>
                <li><a href="<?php echo get_option('home'); ?>/contact" title="Contact">Contact</a></li>
                <li><a href="<?php echo get_option('home'); ?>/employment" title="Employment">Employment</a></li>
                <li><a href="<?php echo get_option('home'); ?>/site-map/" title="Sitemap">Sitemap</a></li>
                <li class="no_border"><a href="https://www.facebook.com/pages/Roman-Village-Cucina-Italiana/109615435737078?sk=info" title="Facebook" id="facebook" target="_blank">Find us on Facebook!</a></li>
			</ul>
        </div>        
    </div><br class="clear_float" /><!-- <div class="footer_wrapper"> -->
    <!--footer wrapper end here-->
    
</div><!--<div class="wrapper">-->
<!--CONTENT WRAPPER Ends-->
<script>
jQuery(document).ready(function($) {
    jQuery('.loading').remove().delay(300).remove();
});

//Smooth Scroll
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>
<!-- Gorgeous design by Michael Heilemann - http://binarybonsai.com/kubrick/ -->
<?php /* "Just what do you think you're doing Dave?" */ ?>




		<?php wp_footer(); ?>
</body>
</html>
