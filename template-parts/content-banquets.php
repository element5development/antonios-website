<?php /*
TEMPALTE FOR DISPLAYING CUSTOM POST TYPE "AFFILIATES"
*/ ?>

<div class="rooms-container">
	<h2 class="location-title">Antonio’s Canton</h2>
	<div class="menu-btns">
		<!-- <a target="_blank" href="/wp-content/uploads/2012/11/Antonios_Canton_Banquet.pdf">Banquet Menu</a>
		<a target="_blank" href="/wp-content/uploads/2012/11/Antonios_Canton_Catering.pdf">Catering Menu</a> -->
	</div>			
	<?php if( have_rows('canton_rooms') ) {
	    while ( have_rows('canton_rooms') ) : the_row(); ?>

	        <div class="single-room"><!--ACF repeater-->
		        <img class="one-half" src="<?php the_sub_field('room_img'); ?>" />
		        <div class="one-half">
					<h3><?php the_sub_field('room_name'); ?></h3>
					<h4>Total seating capacity: <?php the_sub_field('seating_capacity'); ?></h4>
					<p><?php the_sub_field('room_notes'); ?></p>
		        </div>
		        <div style="clear: both"></div>
	        </div>
	        <hr>

	    <?php endwhile;
	} else {
	    // no rows found
	} ?>
	<div style="clear: both"></div>

	<h2 class="location-title">Antonio’s Dearborn Heights</h2>
	<div class="menu-btns">
<!-- 		<a target="_blank" href="/wp-content/uploads/2012/11/Antonios_DearbornHts_Banquet.pdf">Banquet Menu</a>
		<a target="_blank" href="/wp-content/uploads/2012/11/Antonios_DearbornHts_Catering.pdf">Catering Menu</a> -->
	</div>			
	<?php if( have_rows('dearborn_rooms') ) {
	    while ( have_rows('dearborn_rooms') ) : the_row(); ?>

	        <div class="single-room"><!--ACF repeater-->
		        <img class="one-half" src="<?php the_sub_field('room_img'); ?>" />
		        <div class="one-half">
					<h3><?php the_sub_field('room_name'); ?></h3>
					<h4>Total seating capacity: <?php the_sub_field('seating_capacity'); ?></h4>
					<p><?php the_sub_field('room_notes'); ?></p>
		        </div>
		        <div style="clear: both"></div>
	        </div>
	        <hr>

	    <?php endwhile;
	} else {
	    // no rows found
	} ?>
	<div style="clear: both"></div>

	<h2 class="location-title">Antonio's Farmington Hills</h2>
	<div class="menu-btns">
<!-- 		<a target="_blank" href="/wp-content/uploads/2012/11/Antonio_FarmHills_Banquet.pdf">Banquet Menu</a>
		<a target="_blank" href="/wp-content/uploads/2012/11/Antonios_FarmHills_Catering.pdf">Catering Menu</a> -->
	</div>			
	<?php if( have_rows('farmington_rooms') ) {
	    while ( have_rows('farmington_rooms') ) : the_row(); ?>

	        <div class="single-room"><!--ACF repeater-->
		        <img class="one-half" src="<?php the_sub_field('room_img'); ?>" />
		        <div class="one-half">
					<h3><?php the_sub_field('room_name'); ?></h3>
					<h4>Total seating capacity: <?php the_sub_field('seating_capacity'); ?></h4>
					<p><?php the_sub_field('room_notes'); ?></p>
		        </div>
		        <div style="clear: both"></div>
	        </div>
	        <hr>

	    <?php endwhile;
	} else {
	    // no rows found
	} ?>
	<div style="clear: both"></div>

	<h2 class="location-title">Roman Village Dearborn</h2>
	<div class="menu-btns">
<!-- 		<a target="_blank" href="/wp-content/uploads/2012/11/Roman_Dearborn_Banquet.pdf">Banquet Menu</a>
		<a target="_blank" href="/wp-content/uploads/2012/11/Roman_Dearborn_Catering.pdf">Catering Menu</a> -->
	</div>			
	<?php if( have_rows('rv_dearborn_rooms') ) {
	    while ( have_rows('rv_dearborn_rooms') ) : the_row(); ?>

	        <div class="single-room"><!--ACF repeater-->
		        <img class="one-half" src="<?php the_sub_field('room_img'); ?>" />
		        <div class="one-half">
					<h3><?php the_sub_field('room_name'); ?></h3>
					<h4>Total seating capacity: <?php the_sub_field('seating_capacity'); ?></h4>
					<p><?php the_sub_field('room_notes'); ?></p>
		        </div>
		        <div style="clear: both"></div>
	        </div>
	        <hr>

	    <?php endwhile;
	} else {
	    // no rows found
	} ?>
	<div style="clear: both"></div>
</div>

<div class="funeral-luncheons">
	<?php the_field( 'funeral_luncheons' ) ?>
</div>

