<?php /*
TEMPALTE FOR DISPLAYING CUSTOM POST TYPE "AFFILIATES"
*/ ?>

<div class="events-container">	
	<?php if( have_rows('event') ) {
	    while ( have_rows('event') ) : the_row(); ?>

	        <div class="single-events"><!--ACF repeater-->
		        <div class="one-third"><img src="<?php the_sub_field('event_photo'); ?>" /></div>
		        <div class="event-info">
					<h2><?php the_sub_field('event_title'); ?></h2>
					<h3>Total seating capacity: <?php the_sub_field('event_date'); ?></h3>
		        </div>
 				<div style="clear: both"></div>
		        <?php the_sub_field('event_description'); ?>
	        </div>
	        <hr>

	    <?php endwhile;
	} else {
	    // no rows found
	} ?>
	<div style="clear: both"></div>
</div>