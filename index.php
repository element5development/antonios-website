<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>
<!--banner start here-->
    <div class="banner_wrapper">    	
      <div class="check_out_menu_button">
        	<a href="#" title="Check Out the Menu"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/check_out_menu_button.jpg" alt="Check Out button" /></a>
      </div>
    </div><!--<div class="banner_wrapper">-->
    <!--banner end here-->
	    <!--body content start here-->
    <div class="body_content_wrapper">
    	<div class="body_column_1">
        	<div class="section_box">
        	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/about_our_family.jpg" alt="" />
			<p>
            <?php query_posts('page_id=2'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(2,554,false); ?>	
			<? endwhile; ?></p>
            <div class="buttons">
            <a href="<?php echo get_option('home'); ?>/about" title="Learn More"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/learn-more-button.png" alt="Learn More" /></a>
            </div>
          </div>
            
            <div class="section_box">
            <img src="<?php bloginfo('stylesheet_directory'); ?>/images/update_promotions.jpg" alt="" />
			<?php query_posts('page_id=46'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(46,136); ?>	
			<? endwhile; ?>
            <div class="buttons">
	            <a href="<?php echo get_option('home'); ?>/updates-promotions" title="Join Club Italiano"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/join_club_italiano.jpg" alt="Join Club Italiano" /></a>
			</div>
            </div>
            
        </div>
                
        <div class="body_column_1">
        	<div class="section_box">
        	<img src="<?php bloginfo('stylesheet_directory'); ?>/images/keeping_with_tradition.jpg" alt="" />			
            <p>
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/tradition_img.jpg" alt="" class="margin_10 float_right" />
            <?php query_posts('page_id=18'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(18,554); ?>	
			<? endwhile; ?>			
			</p>            
            <div class="buttons">
            <a href="<?php echo get_option('home'); ?>/keeping-with-tradition" title="Learn More"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/learn-more-button.png" alt="Learn More" /></a>
            </div>
            </div>
            
            <div class="section_box">
			<img src="<?php bloginfo('stylesheet_directory'); ?>/images/breaking_bread.jpg" alt="" />
			<?php query_posts('page_id=43'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(43,81); ?>	
			<? endwhile; ?>
            <div class="buttons">
            <a href="<?php echo get_option('home'); ?>/breaking-bread" title="Learn More"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/learn-more-button.png" alt="Learn More" /></a>
            </div>
            </div>
            
        </div>
        
        <div class="body_advertisements_wrapper">
        	<a href="<?php echo get_option('home'); ?>/maps-info"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/map_info.jpg" alt="" /></a>
            <div class="blank_height_20">&nbsp;</div>
            <a href="<?php echo get_option('home'); ?>/gift-cards"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/gift_cards.jpg" alt="" /></a>
        </div>
        
</div><!--<div class="body_content_wrapper">-->
    <!--body content end here -->


<?php //get_sidebar(); ?>

<?php get_footer(); ?>
