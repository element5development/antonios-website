<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Menu Catering
*/

get_header(); ?>

    <div class="body_content_wrapper">
    	<div class="single-column-content">

			<div class="menu-switch-btns">
				<a href="/antonios-lunch-menu/">Antonios Lunch</a>
				<a href="/antonios-dinner-menu/">Antonios Dinner</a>
				<a href="/antonios-carry-out/">Antonios Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/antonios-catering-menu/">Antonios Catering</a>
				<a href="/antonios-banquet-menu/">Antonios Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-lunch-menu/">Roman Village Lunch</a>
				<a href="/roman-village-dinner-menu/">Roman Village Dinner</a>
				<a href="/roman-village-carry-out/">Roman Village Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-catering-menu/">Roman Village Catering</a>
				<a href="/roman-village-banquet-menu/">Roman Village Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/wine-list/">Wine List</a>
			</div>

			<div class="menu-container">
				<h1 class="menu-title"><?php echo get_the_title( $ID ); ?></h1>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<div id="Menu" class="single-menu catering">

					<?php if( get_field('antipasto_tray_title') ) { ?>
						<div id="antipasto-trays" class="menu-section"><!--ACF section-->
							<h3><?php the_field('antipasto_tray_title'); ?></h3>
					       	<?php
								if( have_rows('antipasto_tray_item') ) {
								    while ( have_rows('antipasto_tray_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('antipasto_tray_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('salad_title') ) { ?>
						<div id="antipasto-trays" class="menu-section"><!--ACF section-->
							<h3><?php the_field('salad_title'); ?></h3>
					       	<?php
								if( have_rows('salad_item') ) {
								    while ( have_rows('salad_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('salad_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('vegetables_title') ) { ?>
						<div id="vegetables-catering" class="menu-section"><!--ACF section-->
							<h3><?php the_field('vegetables_title'); ?></h3>
					       	<?php
								if( have_rows('vegetables_item') ) {
								    while ( have_rows('vegetables_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('vegetables_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('pasta_title_1') ) { ?>
						<div id="pasta-catering" class="menu-section"><!--ACF section-->
							<h3><?php the_field('pasta_title_1'); ?></h3>
					       	<?php
								if( have_rows('pasta_item') ) {
								    while ( have_rows('pasta_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('pasta_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('stuffed_title') ) { ?>
						<div id="stuffed-catering" class="menu-section"><!--ACF section-->
							<h3><?php the_field('stuffed_title'); ?></h3>
					       	<?php
								if( have_rows('stuffed_item') ) {
								    while ( have_rows('stuffed_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('stuffed_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('main_title') ) { ?>
						<div id="main-catering" class="menu-section"><!--ACF section-->
							<h3><?php the_field('main_title'); ?></h3>
					       	<?php
								if( have_rows('main_item') ) {
								    while ( have_rows('main_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('main_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('dessert_title') ) { ?>
						<div id="catering-dessert" class="menu-section"><!--ACF section-->
						<h3><?php the_field('dessert_title'); ?></h3>
						<?php
							if( have_rows('dessert_item') ) {
							    while ( have_rows('dessert_item') ) : the_row(); ?>
							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('dessert_title'); ?>
										</div>
										<div class="item-price">
											<span class="small-price"><?php the_sub_field('dessert_price'); ?></span>
										</div>
									</div>
						 		<?php endwhile;
							} else {
							    // no rows found
						} ?>
						<div style="clear: both"></div>
						</div>
					<?php } ?>

					<?php if( get_field('bread_title') ) { ?>
						<div id="catering-bread" class="menu-section"><!--ACF section-->
							<h3>Pizza & Bread</h3>
					        <div class="menu-item"><!--ACF repeater-->
								<div class="item-name">
									<?php the_field('bread_title'); ?>
								</div>
								<div class="item-price">
									<span class="small-price">Full: <?php the_field('bread_full_price'); ?></span> | 
									<span class="large-price">Half: <?php the_field('bread_half_price'); ?></span>
								</div>
							</div>
							<div style="clear: both"></div>
						</div>
					<?php } ?>

					<?php if( get_field('pizza_sheet_title') ) { ?>
						<div id="catering-pizza-sheet" class="menu-section"><!--ACF section-->
					        <div class="menu-item"><!--ACF repeater-->
								<div class="item-name">
									<?php the_field('pizza_sheet_title'); ?>
								</div>
								<div class="item-price">
									<span class="small-price"><?php the_field('pizza_sheet_price'); ?></span>
								</div>
								<div class="item-description">
									<?php the_field('pizza_sheet_description'); ?>
								</div>
							</div>
							<div style="clear: both"></div>
						</div>
					<?php } ?>

					<?php if( get_field('calzoni_title') ) { ?>
						<div id="catering-calzoni" class="menu-section"><!--ACF section-->
					        <div class="menu-item"><!--ACF repeater-->
								<div class="item-name">
									<?php the_field('calzoni_title'); ?>
								</div>
								<div class="item-price">
									<span class="small-price"><?php the_field('calzoni_price'); ?></span>
								</div>
								<div class="item-description">
									<?php the_field('calzoni_description'); ?>
								</div>
							</div>
							<div style="clear: both"></div>
						</div>
					<?php } ?>


					<?php if( get_field('side_title') ) { ?>
						<div id="side-catering" class="menu-section"><!--ACF section-->
							<h3><?php the_field('side_title'); ?></h3>
					       	<?php
								if( have_rows('side_item') ) {
								    while ( have_rows('side_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<div class="item-name">
												<?php the_sub_field('tray_title'); ?>
											</div>
											<div class="item-price">
												<span class="small-price">Full: <?php the_sub_field('full_price'); ?></span> | 
											<span class="large-price">Half: <?php the_sub_field('half_price'); ?></span>
											</div>
											<div class="item-description">
												<?php the_sub_field('tray_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('side_description'); ?></div>
						</div>
					<?php } ?>

					<?php if( get_field('pasta_title_2') ) { ?>
						<div id="catering-pasta" class="menu-section"><!--ACF section-->
						<h3><?php the_field('pasta_title_2'); ?></h3>
						<?php
							if( have_rows('pasta_by_pound') ) {
							    while ( have_rows('pasta_by_pound') ) : the_row(); ?>
							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('pasta_title'); ?>
										</div>
										<div class="item-price">
											<span class="small-price"><?php the_sub_field('pasta_price'); ?></span>
										</div>
									</div>
						 		<?php endwhile;
							} else {
							    // no rows found
						} ?>
						<?php
							if( have_rows('pasta_sauce') ) {
							    while ( have_rows('pasta_sauce') ) : the_row(); ?>
							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('sauce_title'); ?>
										</div>
										<div class="item-price">
											<span class="small-price">Pint: <?php the_sub_field('pint_price'); ?></span> | 
											<span class="large-price">Quart: <?php the_sub_field('quart_price'); ?></span>
										</div>
									</div>
						 		<?php endwhile;
							} else {
							    // no rows found
						} ?>
						<div style="clear: both"></div>
						</div>
					<?php } ?>

					
				</div>
			</div>

        </div>   
 

<?php get_footer(); ?>