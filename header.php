<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<?php /* <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> */ ?>

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">


<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/reest.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/antonios.css" rel="stylesheet" type="text/css" />

<!-- Added by Kolin Karchon 4.05.16 -->
<link href="<?php bloginfo('stylesheet_directory'); ?>/css/mobile.css" rel="stylesheet" type="text/css" />

<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<script src="https://code.jquery.com/jquery-1.8.0.min.js"></script>
<?php wp_head(); ?>



<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<link href="<?php bloginfo('stylesheet_directory'); ?>/css/slider.css" rel="stylesheet" type="text/css" />

<?php wp_enqueue_style('thickbox'); ?> <!-- inserting style sheet for Thickbox.  -->
<?php wp_enqueue_script('thickbox'); ?> <!--  including Thickbox javascript. -->
<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.coda-slider-2.0.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/jquery.antonios.js'></script>

</head>
<body <?php body_class(); ?>>

<!--<script type="text/javascript"> 
<!--
if(window.location.search.indexOf('?ViewFullSite=true') === 0)
document.write('<div id="view-mobile"><a href="http://m.antoniosrestaurants.com/">View Mobile Site</a></div>');
//
</script> -->
  



<!--CONTENT WRAPPER STARTS-->
<div class="header-logos-wrap">
	<div class="header-logos-container">
	   <a href="<?php echo get_option('home'); ?>" title="Antonio's Cucina Italiana" class="antonios">
		   <img src="<?php bloginfo('stylesheet_directory'); ?>/images/antonio.png" alt="Antonio" height="83" width="325" />
		</a>
	   <a href="<?php echo get_option('home'); ?>" title="Roman Village" class="roman-village">
		   <img src="<?php bloginfo('stylesheet_directory'); ?>/images/Roman-Village.png" alt="Roman Village" width="320" height="84" />
		</a>        
	</div>  
</div>

<!--navigation wrapper start -->
<div class="nav-holder">
	<div class="navigation-wrapper">
		<div class="navigation">
			<a href="/locations" class="locations">Locations</a>
			<?php wp_nav_menu( array( 'primary' => 'header-menu' ) ); ?>
			
			<?php echo do_shortcode('[responsive_menu]'); ?>
		</div>
	</div>
</div>
<!-- End Navigation wrapper -->


<div class="headertop">
  <div class="wrapper">
   <!--  <div id="music">
    </div> -->
  </div>
</div>

<div class="banner-wrapper">
	<?php if ( is_front_page() ) {	
		echo do_shortcode( '[rev_slider main]' );?>
		<!--<div class="glutenfree">
         <h2>Now serving Gluten Free Pasta and Gluten Free Pizza</h2>
		</div>-->
	<?php  } ; ?>
</div>

<div class="wrapper">
	<!--header wrapper start -->
   <div class="header_wrapper">
      <div class="header_left_part">
               
		<div class="slogan"><!--<img src="<?php bloginfo('stylesheet_directory'); ?>/images/slogan.png" alt="" class="slogan" />--></div>
	</div>        
</div><!--<div class="header_wrapper">-->

<!--header wrapper end -->




