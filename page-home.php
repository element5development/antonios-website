<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Home
*/


get_header(); ?>
<!--banner start here-->
    <div class="banner_wrapper">
	    
   
    </div><!--<div class="banner_wrapper">-->
    <!--banner end here-->
	    <!--body content start here-->
    <div class="body_content_wrapper">
    	<div class="body_column_1">
        	<div class="section_box">
        	<a href="<?php echo get_option('home'); ?>/about-us" title="Learn More"><img src="/wp-content/themes/antonios/images/about_our_family.gif" alt="About our Family" /></a>
			<p>
            <?php query_posts('page_id=212'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(212,1000,false); ?>	
			<? endwhile; ?></p>
            <div class="buttons">
            <a class="red-btn" href="<?php echo get_option('home'); ?>/about-us" title="Learn More">Learn More</a>
            </div>
          </div>
            
            <div class="section_box">
            <a href="<?php echo get_option('home'); ?>/events" title="Learn More"><img src="/wp-content/themes/antonios/images/updates_and_promotions.gif" alt="Updates & Promotions" /></a>
			<p><?php query_posts('page_id=220'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(220,1000); ?>	
			<? endwhile; ?></p>
            <div class="buttons">
	            <a class="red-btn" href="<?php echo get_option('home'); ?>/events" title="Learn More">Learn More</a>
			</div>
            </div>
			<div class="section_box">
            <a href="<?php echo get_option('home'); ?>/contact" title="Join Club Italiano"><img src="/wp-content/themes/antonios/images/join_club_italiano.gif" alt="Join Club Italiano" /></a>
			<p><?php query_posts('page_id=388'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(388,1000); ?>	
			<? endwhile; ?></p>
            <div class="buttons">
	            <a href="/contact" class="red-btn" title="Join Club Italiano">Join Club Italiano</a>
			</div>
            </div>
        </div>
        <div class="body_column_1">
        	<div class="section_box">
        	<a href="<?php echo get_option('home'); ?>/antonios-a-family-tradition-for-50-years" title="Learn More"><img src="/wp-content/themes/antonios/images/keeping_with_tradition.gif" alt="Keeping with Tradition" /></a>		
            <p>
			<img src="/wp-content/themes/antonios/images/tradition_img.jpg" alt="" class="margin_10 float_right" />
            <?php query_posts('page_id=225'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(225,1000); ?>	
			<? endwhile; ?>			
			</p>            
            <div class="buttons">
            <a class="red-btn" href="<?php echo get_option('home'); ?>/antonios-a-family-tradition-for-50-years" title="Learn More">Learn More</a>
            </div>
            </div>
            
            <div class="section_box">
			<a href="<?php echo get_option('home'); ?>/traditions-mama-rita" title="Learn More"><img src="/wp-content/themes/antonios/images/breaking_bread_with_mama_rita.gif" alt="Breaking Bread with Mama Rita" /></a>
			<?php query_posts('page_id=227'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(227,1000); ?>	
			<? endwhile; ?>
            <div class="buttons">
            <a class="red-btn" href="<?php echo get_option('home'); ?>/traditions-mama-rita" title="Learn More">Learn More</a>
            </div>
            </div>
        </div>
        <div class="body_advertisements_wrapper">
        	<a href="<?php echo get_option('home'); ?>/locations"><img src="/wp-content/uploads/2016/07/map_info-updated.png" alt="Contact Info" /></a>
            <div class="blank_height_20">&nbsp;</div>
            <a href="https://antoniosrestaurants.localgiftcards.com/"><img src="/wp-content/themes/antonios/images/gift_cards.png" alt="Give the Gift of Food - Gift Cards" /></a>
        </div>
</div><!--<div class="body_content_wrapper">-->
    <!--body content end here -->


<?php //get_sidebar(); ?>

<?php get_footer(); ?>
