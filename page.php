<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

get_header(); ?>
   	<!--body content start here-->
	 	<div class="body_content_wrapper">
    		<!-- interior_body_left start -->
	 		<div class="interior_body_left">
	 		<!--TITLE-->
				<h1><?php the_title(); ?></h1>
			<!--MAIN CONTENT-->
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			<!--BANQUET & CATERING -->
				<?php if ( is_page(7) ) { ?>
					<?php get_template_part( 'template-parts/content', 'banquets' ); ?>
				<?php } ?> 
			<!--SAVE THE DATE -->
				<?php if ( is_page(37) ) { ?>
					<?php get_template_part( 'template-parts/content', 'events' ); ?>
				<?php } ?> 
	      	</div><!--<div class="interior_body_left">-->
			<!-- interior_body_left start -->      
			<?php get_sidebar(); ?>

<?php get_footer(); ?>
