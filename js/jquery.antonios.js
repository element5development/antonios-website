jQuery(document).ready(function($) {
	$('a.lightbox').colorbox({rel:'group1'});
	$('#coda-slider-1').codaSlider({
		crossLinking: false,
		dynamicArrows: false
	});
	$('#coda-slider-2').codaSlider({
		crossLinking: false,
		dynamicArrows: false
	});

	$('p.loading').remove();
	$('#romanMenu').hide();

	$("#showAntonioMenu").click( function() {
			$("#romanMenu").hide();
			$("#antonioMenu").show();
			$('#pageTitle').html("Menu > Antonio's");
	});
	$("#showRomanMenu").click( function() {
			$("#antonioMenu").hide();
			$("#romanMenu").show();
			$('#pageTitle').html("Menu > Roman Village");
	});

});

jQuery(document).ready(function($) {
	jQuery('.js-accordion-trigger').bind('click', function(e){
	  jQuery(this).parent().find('.submenu').slideToggle('fast');  // apply the toggle to the ul
	  jQuery(this).parent().toggleClass('is-expanded');
	  e.preventDefault();
	});
});