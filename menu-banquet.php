<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Menu Banquet
*/

get_header(); ?>

    <div class="body_content_wrapper">
    	<div class="single-column-content">

			<div class="menu-switch-btns">
				<a href="/antonios-lunch-menu/">Antonios Lunch</a>
				<a href="/antonios-dinner-menu/">Antonios Dinner</a>
				<a href="/antonios-carry-out/">Antonios Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/antonios-catering-menu/">Antonios Catering</a>
				<a href="/antonios-banquet-menu/">Antonios Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-lunch-menu/">Roman Village Lunch</a>
				<a href="/roman-village-dinner-menu/">Roman Village Dinner</a>
				<a href="/roman-village-carry-out/">Roman Village Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-catering-menu/">Roman Village Catering</a>
				<a href="/roman-village-banquet-menu/">Roman Village Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/wine-list/">Wine List</a>
			</div>

			<div class="menu-container">
				<h1 class="menu-title"><?php echo get_the_title( $ID ); ?></h1>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<div id="Menu" class="single-menu catering">

					<?php if( get_field('banquet_item') ) { ?>
						<div id="banquet" class="menu-section"><!--ACF section-->
					       	<?php
								if( have_rows('banquet_item') ) {
								    while ( have_rows('banquet_item') ) : the_row(); ?>
								        <div class="menu-item"><!--ACF repeater-->
											<h3>
												<?php the_sub_field('item_title'); ?>
											</h3>
											<div class="item-price">
												<span class="small-price"><?php the_sub_field('item_price'); ?> per person</span>
											</div>
											<div class="item-description">
												<?php the_sub_field('item_description'); ?>
											</div>
										</div>
							 		<?php endwhile;
								} else {
								    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					
				</div>
			</div>

        </div>   
 

<?php get_footer(); ?>