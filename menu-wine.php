<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Menu Wine
*/

get_header(); ?>

    <div class="body_content_wrapper">
    	<div class="single-column-content">

			<div class="menu-switch-btns">
				<a href="/antonios-lunch-menu/">Antonios Lunch</a>
				<a href="/antonios-dinner-menu/">Antonios Dinner</a>
				<a href="/antonios-carry-out/">Antonios Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/antonios-catering-menu/">Antonios Catering</a>
				<a href="/antonios-banquet-menu/">Antonios Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-lunch-menu/">Roman Village Lunch</a>
				<a href="/roman-village-dinner-menu/">Roman Village Dinner</a>
				<a href="/roman-village-carry-out/">Roman Village Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-catering-menu/">Roman Village Catering</a>
				<a href="/roman-village-banquet-menu/">Roman Village Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/wine-list/">Wine List</a>
			</div>


			<div class="menu-container">
				<h1 class="menu-title"><?php echo get_the_title( $ID ); ?></h1>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<div id="Menu" class="single-menu">
					<h3>RED WINE</h3>
					<?php if( get_field('vini_rossi_d’italia') ) { ?>
						<div id="red-wine-1" class="menu-section"><!--ACF section-->
							<h3>Vini Rossi d’Italia</h3>
							<?php
							if( have_rows('vini_rossi_d’italia') ) {
							    while ( have_rows('vini_rossi_d’italia') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('nuovo') ) { ?>
						<div id="red-wine-2" class="menu-section"><!--ACF section-->
							<h3>Vini Rossi del Mondo Nuovo</h3>
							<?php
							if( have_rows('nuovo') ) {
							    while ( have_rows('nuovo') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('vecchio') ) { ?>
						<div id="red-wine-3" class="menu-section"><!--ACF section-->
							<h3>Vini Rossi del Mondo Vecchio</h3>
							<?php
							if( have_rows('vecchio') ) {
							    while ( have_rows('vecchio') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<h3>White Wine</h3>
					<?php if( get_field('d’Italia') ) { ?>
						<div id="white-wine-1" class="menu-section"><!--ACF section-->
							<h3>Vini Bianchi d’Italia</h3>
							<?php
							if( have_rows('d’Italia') ) {
							    while ( have_rows('d’Italia') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('white_nuovo') ) { ?>
						<div id="white-wine-2" class="menu-section"><!--ACF section-->
							<h3>Vini Bianchi del Mondo Nuovo</h3>
							<?php
							if( have_rows('white_nuovo') ) {
							    while ( have_rows('white_nuovo') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('white_vecchio') ) { ?>
						<div id="white-wine-3" class="menu-section"><!--ACF section-->
							<h3>Vini Bianchi del Mondo Vecchio</h3>
							<?php
							if( have_rows('white_vecchio') ) {
							    while ( have_rows('white_vecchio') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('house_wines_title') ) { ?>
						<div id="house-wine" class="menu-section"><!--ACF section-->
							<h3><?php the_field('house_wines_title'); ?></h3>
							<div class="section-description"><?php the_field('house_wine_description'); ?></div>
							<?php if( have_rows('red_wine_item') ) { ?>
								<ul class="one-half">
							        	<li><b>Red Wine</b></li>
							    <?php while ( have_rows('red_wine_item') ) : the_row(); ?>
							        	<li><?php the_sub_field('name'); ?></li>
							    <?php endwhile; ?>
							    </ul>
							<?php } ?>
							<?php if( have_rows('white_wine_item') ) { ?>
								<ul class="one-half">
							    		<li><b>White Wine</b></li>
							    <?php while ( have_rows('white_wine_item') ) : the_row(); ?>
							        	<li><?php the_sub_field('name'); ?></li>
							    <?php endwhile; ?>
								</ul>
							<?php } ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>

					<?php if( get_field('champagne_e_spumanti') ) { ?>
						<div id="white-wine-3" class="menu-section"><!--ACF section-->
							<h3>MPAGNE E SPUMANTI</h3>
							<?php
							if( have_rows('champagne_e_spumanti') ) {
							    while ( have_rows('champagne_e_spumanti') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_split') ) { ?>
												<span class="small-price">Split: <?php the_sub_field('item_split'); ?></span> | 
											<?php } ?>
											<?php if( get_sub_field('item_bottle') ) { ?>
												<span class="large-price">Bottle: <?php the_sub_field('item_bottle'); ?></span>
											<?php } ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>

					<?php if( get_field('cena') ) { ?>
						<div id="white-wine-3" class="menu-section"><!--ACF section-->
							<h3>DOPO LA CENA</h3>
							<?php
							if( have_rows('cena') ) {
							    while ( have_rows('cena') ) : the_row(); ?>

							        <div class="menu-item"><!--ACF repeater-->
										<div class="item-name">
										<?php the_sub_field('item_name'); ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('item_glass') ) { ?>
												<span class="small-price">Glass: <?php the_sub_field('item_glass'); ?></span> 
											<?php } ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					
				</div>
			</div>

        </div>   
 

<?php get_footer(); ?>