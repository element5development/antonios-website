<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Menu
*/

get_header(); ?>

    <div class="body_content_wrapper">
    	<div class="single-column-content">

			<div class="menu-switch-btns">
				<a href="/antonios-lunch-menu/">Antonios Lunch</a>
				<a href="/antonios-dinner-menu/">Antonios Dinner</a>
				<a href="/antonios-carry-out/">Antonios Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/antonios-catering-menu/">Antonios Catering</a>
				<a href="/antonios-banquet-menu/">Antonios Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-lunch-menu/">Roman Village Lunch</a>
				<a href="/roman-village-dinner-menu/">Roman Village Dinner</a>
				<a href="/roman-village-carry-out/">Roman Village Carry Out</a>
			</div>
			<div class="menu-switch-btns">
				<a href="/roman-village-catering-menu/">Roman Village Catering</a>
				<a href="/roman-village-banquet-menu/">Roman Village Banquet</a> 
			</div>
			<div class="menu-switch-btns">
				<a href="/wine-list/">Wine List</a>
			</div>


			<div class="menu-container">
				<h1 class="menu-title"><?php echo get_the_title( $ID ); ?></h1>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
				<div id="Menu" class="single-menu">
					<?php if( get_field('appetizer_menu_title') ) { ?>
						<div id="sides" class="menu-section"><!--ACF section-->
							<h3><?php the_field('appetizer_menu_title'); ?></h3>
							<img src="<?php the_field('appetizer_menu_img'); ?>" class="menu-img one-half right" />
							<?php
							if( have_rows('appetizer_item') ) {
							    while ( have_rows('appetizer_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('appetizer_item_name'); ?>
											<?php if( get_sub_field('appetizer_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('appetizer_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('appetizer_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('appetizer_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>

					<?php if( get_field('Soup_menu_title') ) { ?>
						<div id="soups" class="menu-section"><!--ACF section-->
							<h3><?php the_field('Soup_menu_title'); ?></h3>
							<?php
							if( have_rows('Soup_item') ) {
							    while ( have_rows('Soup_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('soup_item_name'); ?>
											<?php if( get_sub_field('soup_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('soup_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<span class="small-price"><?php the_sub_field('soup_item_price_sm'); ?></span>  
											<span class="large-price"><?php the_sub_field('soup_item_price_lg'); ?></span>
										</div>
										<div class="item-description">
											<?php the_sub_field('soup_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('soup_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('salad_menu_title') ) { ?>
						<div id="salads" class="menu-section"><!--ACF section-->
							<h3><?php the_field('salad_menu_title'); ?></h3>
							<img src="<?php the_field('salad_menu_img'); ?>" class="menu-img one-half" />
							<?php
							if( have_rows('salad _item') ) {
							    while ( have_rows('salad _item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('salad_item_name'); ?>
											<?php if( get_sub_field('salad_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('salad_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('salad_item_price_one_size') ) { ?>
												<span class="small-price"><?php the_sub_field('salad_item_price_one_size'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('salad_item_price_sm') ) { ?>
												<span class="small-price">sm: <?php the_sub_field('salad_item_price_sm'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('salad_item_price_med') ) { ?>
												| <span class="small-price">med: <?php the_sub_field('salad_item_price_med'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('salad_item_price_lg') ) { ?>
												| <span class="large-price">lg: <?php the_sub_field('salad_item_price_lg'); ?></span>
											<?php } ?> 
										</div>
										<?php if( get_sub_field('salad_item_description') ) { ?>
											<div class="item-description">
												<?php the_sub_field('salad_item_description'); ?>
											</div>
										<?php } ?> 
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('salad_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('panini_menu_title') ) { ?>
						<div id="panini" class="menu-section"><!--ACF section-->
							<h3><?php the_field('panini_menu_title'); ?></h3>
							<img src="<?php the_field('panini_menu_img'); ?>" class="menu-img one-half right" />
							<?php
							if( have_rows('panini_item') ) {
							    while ( have_rows('panini_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('panini_item_name'); ?>
											<?php if( get_sub_field('panini_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('panini_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<span class="price"><?php the_sub_field('panini_item_price'); ?></span> 
										</div>
										<div class="item-description">
											<?php the_sub_field('panini_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('panini_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('pizza_menu_title') ) { ?>
						<div id="pizza" class="menu-section"><!--ACF section-->
							<h3><?php the_field('pizza_menu_title'); ?></h3>
							<img src="<?php the_field('pizza_menu_img'); ?>" class="menu-img one-half" />
							<?php
							if( have_rows('pizza_item') ) {
							    while ( have_rows('pizza_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('pizza_item_name'); ?>
											<?php if( get_sub_field('pizza_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('pizza_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('pizza_item_price_sm') ) { ?>
												<span class="small-price">sm: <?php the_sub_field('pizza_item_price_sm'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('pizza_item_price_med') ) { ?>
												| <span class="small-price">lg: <?php the_sub_field('pizza_item_price_med'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('pizza_item_price_lg') ) { ?>
												| <span class="large-price">oval: <?php the_sub_field('pizza_item_price_lg'); ?></span>
											<?php } ?> 
										</div>
										<?php if( get_sub_field('pizza_item_description') ) { ?>
											<div class="item-description">
												<?php the_sub_field('pizza_item_description'); ?>
											</div>
										<?php } ?> 
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('calzoni_menu_title') ) { ?>
						<div id="calzoni" class="menu-section"><!--ACF section-->
						<h3><?php the_field('calzoni_menu_title'); ?></h3>
							<img src="<?php the_field('calzoni_menu_img'); ?>" class="menu-img one-half right" />
							<?php
							if( have_rows('calzoni_item') ) {
							    while ( have_rows('calzoni_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('calzoni_item_name'); ?>
											<?php if( get_sub_field('calzoni_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('calzoni_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php if( get_sub_field('calzoni_item_price_sm') ) { ?>
												<span class="small-price">Bambino: <?php the_sub_field('calzoni_item_price_sm'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('calzoni_item_price_med') ) { ?>
												| <span class="small-price">Mamma: <?php the_sub_field('calzoni_item_price_med'); ?></span> 
											<?php } ?> 
											<?php if( get_sub_field('calzoni_item_price_lg') ) { ?>
												| <span class="large-price">Papa: <?php the_sub_field('calzoni_item_price_lg'); ?></span>
											<?php } ?> 
										</div>
										<?php if( get_sub_field('calzoni_item_description') ) { ?>
											<div class="item-description">
												<?php the_sub_field('calzoni_item_description'); ?>
											</div>
										<?php } ?> 
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('calzoni_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('pasta_menu_title') ) { ?>
						<div id="pasta" class="menu-section"><!--ACF section-->
							<h3><?php the_field('pasta_menu_title'); ?></h3>
							<img src="<?php the_field('pasta_menu_img'); ?>" class="menu-img one-half" />
							<?php
							if( have_rows('pasta_item') ) {
							    while ( have_rows('pasta_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('pasta_item_name'); ?>
											<?php if( get_sub_field('pasta_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('pasta_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('pasta_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('pasta_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('pasta_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('stuffed_pasta_menu_title') ) { ?>
						<div id="stuffed-pasta" class="menu-section"><!--ACF section-->
							<h3><?php the_field('stuffed_pasta_menu_title'); ?></h3>
							<img src="<?php the_field('stuffed_pasta_menu_img'); ?>" class="menu-img one-half right" />
							<?php
							if( have_rows('stuffed_pasta_item') ) {
							    while ( have_rows('stuffed_pasta_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('stuffed_pasta_item_name'); ?>
											<?php if( get_sub_field('stuffed_pasta_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('stuffed_pasta_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('stuffed_pasta_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('stuffed_pasta_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('stuffed_pasta_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('seafood_pasta_menu_title') ) { ?>
						<div id="seafood-pasta" class="menu-section"><!--ACF section-->
							<h3><?php the_field('seafood_pasta_menu_title'); ?></h3>
							<img src="<?php the_field('seafood_pasta_menu_img'); ?>" class="menu-img one-half" />
							<?php
							if( have_rows('seafood_pasta_item') ) {
							    while ( have_rows('seafood_pasta_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('seafood_pasta_item_name'); ?>
											<?php if( get_sub_field('seafood_pasta_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('seafood_pasta_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('seafood_pasta_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('seafood_pasta_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('seafood_pasta_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('gluten_free_pasta_menu_title') ) { ?>
						<div id="gluten-free-pasta" class="menu-section"><!--ACF section-->
							<h3><?php the_field('gluten_free_pasta_menu_title'); ?></h3>
							<?php
							if( have_rows('gluten_free_pasta_item') ) {
							    while ( have_rows('gluten_free_pasta_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('gluten_free_pasta_item_name'); ?>
											<?php if( get_sub_field('gluten_free_pasta_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('gluten_free_pasta_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('gluten_free_pasta_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('gluten_free_pasta_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('gluten_free_pasta_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('side_menu_title') ) { ?>
					<div id="sides" class="menu-section"><!--ACF section-->
							<h3><?php the_field('side_menu_title'); ?></h3>
							<?php
							if( have_rows('side_item') ) {
							    while ( have_rows('side_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('side_item_name'); ?>
											<?php if( get_sub_field('side_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('side_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('side_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('side_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('side_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('child_menu_title') ) { ?>
						<div id="childern" class="menu-section"><!--ACF section-->
							<h3><?php the_field('child_menu_title'); ?></h3>
							<?php
							if( have_rows('child_item') ) {
							    while ( have_rows('child_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('child_item_name'); ?>
											<?php if( get_sub_field('child_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('child_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('child_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('child_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('Child_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('main_menu_title') ) { ?>
						<div id="main-course" class="menu-section"><!--ACF section-->
							<h3><?php the_field('main_menu_title'); ?></h3>
							<img src="<?php the_field('main_menu_img'); ?>" class="menu-img one-half" />
							<?php
							if( have_rows('main_item') ) {
							    while ( have_rows('main_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('main_item_name'); ?>
											<?php if( get_sub_field('main_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('main_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('main_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('main_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('main_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('seafood_menu_title') ) { ?>
						<div id="seafood" class="menu-section"><!--ACF section-->
							<h3><?php the_field('seafood_menu_title'); ?></h3>
							<img src="<?php the_field('seafood_menu_img'); ?>" class="menu-img one-half right" />
							<?php
							if( have_rows('seafood_item') ) {
							    while ( have_rows('seafood_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('seafood_item_name'); ?>
											<?php if( get_sub_field('seafood_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('seafood_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('seafood_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('seafood_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('seafood_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('seafood_menu_title') ) { ?>
						<div id="dessert" class="menu-section"><!--ACF section-->
							<h3><?php the_field('dessert_menu_title'); ?></h3>
							<?php
							if( have_rows('dessert_item') ) {
							    while ( have_rows('dessert_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('dessert_item_name'); ?>
											<?php if( get_sub_field('dessert_item_vegetarian') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2012/11/veg.png" />
											<?php } ?>
											<?php if( get_sub_field('dessert_item_gluten_free') == 'yes' ) { ?>
												<img src="/wp-content/uploads/2014/04/glutenfree_icon_01-e1398702318201.png" />
											<?php } ?>
										</div>
										<div class="item-price">
											<?php the_sub_field('dessert_item_price'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('dessert_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('dessert_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('sdrink_menu_title') ) { ?>
						<div id="specialty-drinks" class="menu-section"><!--ACF section-->
							<h3><?php the_field('sdrink_menu_title'); ?></h3>
							<img src="<?php the_field('specialty_drinks_menu_image'); ?>" class="menu-img one-half right" />
							<?php
							if( have_rows('sdrink_item') ) {
							    while ( have_rows('sdrink_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('sdrink_item_name'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('sdrink_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('icedrink_menu_title') ) { ?>
						<div id="ice-cream-drinks" class="menu-section"><!--ACF section-->
							<h3><?php the_field('icedrink_menu_title'); ?></h3>
							<?php
							if( have_rows('icedrink_item') ) {
							    while ( have_rows('icedrink_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('icedrink_item_name'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('icedrink_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('hotdrink_menu_title') ) { ?>
						<div id="hot-drinks" class="menu-section"><!--ACF section-->
							<h3><?php the_field('hotdrink_menu_title'); ?></h3>
							<?php
							if( have_rows('hotdrink_item') ) {
							    while ( have_rows('hotdrink_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('hotdrink_item_name'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('hotdrink_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
					<?php if( get_field('espresso_menu_title') ) { ?>
						<div id="espresso" class="menu-section"><!--ACF section-->
							<h3><?php the_field('espresso_menu_title'); ?></h3>
							<?php
							if( have_rows('espresso_item') ) {
							    while ( have_rows('espresso_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('espresso_item_name'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('espresso_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('wine_menu_title') ) { ?>
						<div id="house-wines" class="menu-section"><!--ACF section-->
							<h3><?php the_field('wine_menu_title'); ?></h3>
							<?php
							if( have_rows('wine_item') ) {
							    while ( have_rows('wine_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('wine_item_name'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('wine_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('beer_menu_title') ) { ?>
						<div id="bottled-beer" class="menu-section"><!--ACF section-->
						<h3><?php the_field('beer_menu_title'); ?></h3>
							<?php
							if( have_rows('beer_item') ) {
							    while ( have_rows('beer_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('beer_item_name'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
							<div class="section-description"><?php the_field('beer_menu_description'); ?></div>
						</div>
					<?php } ?>
					<?php if( get_field('non_menu_title') ) { ?>
						<div id="non-alcoholic" class="menu-section"><!--ACF section-->
						<h3><?php the_field('non_menu_title'); ?></h3>
							<?php
							if( have_rows('non_item') ) {
							    while ( have_rows('non_item') ) : the_row(); ?>

							        <div class="menu-item one-half"><!--ACF repeater-->
										<div class="item-name">
											<?php the_sub_field('non_item_name'); ?>
										</div>
										<div class="item-description">
											<?php the_sub_field('non_item_description'); ?>
										</div>
									</div>

							    <?php endwhile;
							} else {
							    // no rows found
							} ?>
							<div style="clear: both"></div>
						</div>
					<?php } ?>
				</div>
			</div>

        </div>   
 

<?php get_footer(); ?>