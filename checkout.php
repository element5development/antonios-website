<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Checkout
*/


get_header(); ?>
    <!--body content start here-->
    <div class="body_content_wrapper">
        <!-- interior_body_left start -->
        <div class="interior_body_left">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entry">

				<div id="jcart"><?php wp_jcart($location = 'content');?></div>

				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>
		</div>
		<?php endwhile; endif; ?>

        <?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>

        </div><!--<div class="interior_body_left">-->
        <!-- interior_body_left start -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
