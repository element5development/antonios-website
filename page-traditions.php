<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 * Template Name: Traditions
 */

get_header(); ?>
    <!--body content start here-->
    <div class="body_content_wrapper">
    	<!-- interior_body_left start -->
    	<div class="interior_body_left">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
				<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
    
    
    <h2>Breaking Bread with Mama Rita</h2>
	<?php
		query_posts('category_name=mama-rita');
		if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>
                        <h3><?php the_title(); ?></h3>
			<?php global $more; ?>
			<?php $more = 0; ?>
                        <?php the_content('<nobr>[ Read More ]</nobr>'); ?>
	<?php endwhile; endif; wp_reset_query(); ?>


        </div><!--<div class="interior_body_left">-->
        <!-- interior_body_left start -->      
                


<?php get_sidebar(); ?>

<?php get_footer(); ?>
