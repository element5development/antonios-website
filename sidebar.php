<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<div class="interior_body_right">
<div id="jcart"><?php wp_jcart($location = 'sidebar');?></div>
<?php if(!is_page('gift-cards')): ?>
            <div class="body_advertisements_wrapper">
                <a href="https://antoniosrestaurants.localgiftcards.com/"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/gift_cards.png" alt="" /></a>
            </div>
<?php endif; ?>
            
				<div class="side_bar_page_summary">
            	<h3>Keeping with Tradition</h3>
                <p><img src="<?php bloginfo('stylesheet_directory'); ?>/images/interior_keeping_with_tradition.jpg" alt="" class="float_right" />
				<?php query_posts('page_id=225'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(225,1000); ?>
				<? endwhile; ?>
				</p>
             <div class="buttons">
            <a class="red-btn" href="<?php echo get_option('home'); ?>/antonios-a-family-tradition-for-50-years" title="Learn More">Learn More</a>
            </div>
            </div>
          <div class="side_bar_page_summary">
            <h3>Updates & Promotions</h3>
			<p><?php query_posts('page_id=220'); while (have_posts()) : the_post(); ?>
				<?php echo getPageContent(220,1000); ?>	
			<? endwhile; ?></p>
            <div class="buttons">
	            <a class="red-btn" href="<?php echo get_option('home'); ?>/events" title="Learn More">Learn More</a>
			</div>
            </div>  	
		</div>
		</div><!--<div class="body_content_wrapper">-->
    <!--body content end here -->
