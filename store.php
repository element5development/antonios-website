<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

/*
Template Name: Store
*/

get_header(); ?>
    <!--body content start here-->
    <div class="body_content_wrapper">
    	<!-- interior_body_left start -->
    	<div class="interior_body_left">

<?php $gcAmounts = array(25,40,50,100,200); ?>

<script type="text/javascript">
jQuery(document).ready( function($) {
	jQuery("select[name=my-item-price-select]").change( function(){ 
		var element = jQuery(this).parentsUntil("form").children("input[name=my-item-id]");
		var elementDefaultVal = element.siblings("input[name=my-item-id-default]").val();
		var elementDefaultNameVal = element.siblings("input[name=my-item-name-default]").val();
		var price = jQuery(this).val().substring(0, jQuery(this).val().length-3);
		element.val(elementDefaultVal + '_' + price);
		element.siblings("input[name=my-item-price]").val($(this).val());
		element.siblings("input[name=my-item-name]").val(elementDefaultNameVal + ' - $' + price);
	});
});
</script>


			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<h1><?php the_title(); ?></h1>
			
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
				<?php endwhile; endif; ?>


<div style="width: 210px; float: left;">
				<form method="post" action="" class="jcart">
					<img src="<?php echo bloginfo('template_directory'); ?>/images/gc-a.png" />
					<fieldset>
						<input type="hidden" name="my-item-id-default" value="1" />
						<input type="hidden" name="my-item-id" value="1_<?php echo $gcAmounts[0]; ?>" />
						<input type="hidden" name="my-item-qty" value="1" />
						<input type="hidden" name="my-item-price" value="<?php echo number_format($gcAmounts[0],2); ?>" />
						<input type="hidden" name="my-item-name-default" value="Original" />
						<input type="hidden" name="my-item-name" value="Original - $<?php echo $gcAmounts[0]; ?>" />
						<ul>
							<li><strong>Original</strong></li>
							<li>
									<select name="my-item-price-select">
									<?php foreach($gcAmounts as $amt): ?>
										<option value="<?php echo number_format($amt,2); ?>">$<?php echo number_format($amt,2); ?></option>
									<?php endforeach; ?>
									</select>
							</li>
						</ul>

						<div style="width: 210px; text-align: center;">
							<input type="image" src="<?php echo bloginfo('template_directory'); ?>/images/btn-add-to-cart.gif" name="my-add-button" value="add to cart" class="jcart-button button" />
						</div>
					</fieldset>
				</form>
</div>
<!-- hide this giftcard
<div style="width: 210px; float: left;">

				<form method="post" action="" class="jcart">
					<img src="<?php echo bloginfo('template_directory'); ?>/images/gc-b.png" />
					<fieldset>
						<input type="hidden" name="my-item-id-default" value="2" />
						<input type="hidden" name="my-item-id" value="2_<?php echo $gcAmounts[0]; ?>" />
						<input type="hidden" name="my-item-qty" value="1" />
						<input type="hidden" name="my-item-price" value="<?php echo number_format($gcAmounts[0],2); ?>" />
						<input type="hidden" name="my-item-name-default" value="Celebration" />
						<input type="hidden" name="my-item-name" value="Celebration - $<?php echo $gcAmounts[0]; ?>" />
						<ul>
							<li><strong>Celebration</strong></li>
							<li> 
									<select name="my-item-price-select">
									<?php foreach($gcAmounts as $amt): ?>
										<option value="<?php echo number_format($amt,2); ?>">$<?php echo number_format($amt,2); ?></option>
									<?php endforeach; ?>
									</select>
							</li>
						</ul>

						<div style="width: 210px; text-align: center;">
							<input type="image" src="<?php echo bloginfo('template_directory'); ?>/images/btn-add-to-cart.gif" name="my-add-button" value="add to cart" class="jcart-button" />
						</div>
					</fieldset>
				</form>
</div>
-->
<!-- hide this giftcard
<div style="width: 210px; float: left;">

				<form method="post" action="" class="jcart">
					<img src="<?php echo bloginfo('template_directory'); ?>/images/gc-c.png" />
					<fieldset>
						<input type="hidden" name="my-item-id-default" value="3" />
						<input type="hidden" name="my-item-id" value="3_<?php echo $gcAmounts[0]; ?>" />
						<input type="hidden" name="my-item-qty" value="1" />
						<input type="hidden" name="my-item-price" value="<?php echo number_format($gcAmounts[0],2); ?>" />
						<input type="hidden" name="my-item-name-default" value="Holiday" />
						<input type="hidden" name="my-item-name" value="Holiday - $<?php echo $gcAmounts[0]; ?>" />
						<ul>
							<li><strong>Holiday</strong></li>
							<li>
									<select name="my-item-price-select">
									<?php foreach($gcAmounts as $amt): ?>
										<option value="<?php echo number_format($amt,2); ?>">$<?php echo number_format($amt,2); ?></option>
									<?php endforeach; ?>
									</select>
							</li>
						</ul>

						<div style="width: 210px; text-align: center;">
							<input type="image" src="<?php echo bloginfo('template_directory'); ?>/images/btn-add-to-cart.gif" name="my-add-button" value="add to cart" class="jcart-button" />
						</div>
					</fieldset>
				</form>
</div>
-->
<div class="clear"></div>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
    
    
        </div><!--<div class="interior_body_left">-->
        <!-- interior_body_left start -->      
                


<?php get_sidebar(); ?>

<?php get_footer(); ?>
